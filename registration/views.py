from django.template import RequestContext
from django.shortcuts import redirect, render
from django.shortcuts import render_to_response
from django.http import HttpResponse, HttpResponseRedirect
from django.template.loader import render_to_string
from django.contrib.auth.decorators import login_required
from django.contrib.auth import get_user_model
from django.core.urlresolvers import reverse

from django.conf import settings
from django.contrib.auth.views import login as auth_login
from django.contrib.auth import login as do_login
from django.views.generic import View, UpdateView, ListView, FormView
from django.contrib.sites.models import RequestSite, Site

from django.core.mail import send_mail, EmailMessage
from braces.views import LoginRequiredMixin, SuperuserRequiredMixin

from registration.forms import UserCreationForm, UserAuthenticationForm
from registration import signals
from django.utils import timezone

User = get_user_model


def login(request, *args, **kwargs):
    
    '''
    Overrides the Django's login to incude `Remember Me` functionality.
    '''
    
    if request.method == 'POST':
        login_form = UserAuthenticationForm(request, data = request.POST)
        get_remember = request.POST.get('remember_me', None)
        
        if login_form.is_valid():
            auth_email = login_form.cleaned_data['username']
            user = User().objects.get(email = auth_email)
            
            if get_remember:
                remember_user(request, user)
                
            '''
            if not get_remember:
                request.session.set_expiry(1209600) # 2 weeks (In Seconds)
            '''
            
    return auth_login(request, *args, **kwargs)


def register(request, success_url='registration_complete',
             template_name='registration/registration_form.html',
             extra_context=None):
    '''
    Allow a new user to register an account.
    '''

    if request.method == 'POST':
        form = UserCreationForm(data=request.POST, files=request.FILES)
        if form.is_valid():
            cleaned_data = form.cleaned_data
            
            # Create New User
            first_name = cleaned_data['first_name']
            last_name = cleaned_data['last_name']
            username = '%s %s' %(first_name, last_name)
            
            new_user = User().objects.create_inactive_user(cleaned_data['email'], cleaned_data['password1'], first_name=cleaned_data['first_name'])
            new_user.username = username
            new_user.last_name = last_name
            new_user.first_name = first_name
            new_user.user_type = form.cleaned_data['user_type']
            new_user.save()
            
            signals.user_registered.send(sender=User(),
                                         user=new_user,
                                         request=request)

            return redirect(success_url)
    else:
        form = UserCreationForm()

    if extra_context is None:
        extra_context = {}
    context = RequestContext(request)
    for key, value in extra_context.items():
        context[key] = callable(value) and value() or value

    return render_to_response(template_name,
                              {'form': form},
                              context_instance=context)

def register_complete(request,  *args, **kwargs):
    if request.method == 'POST':
        login_form = UserAuthenticationForm(request, data = request.POST)
    
        if login_form.is_valid():
            auth_email = login_form.cleaned_data['username']
            user = User().objects.get(email = auth_email)
            return auth_login(request, *args, **kwargs)
	else:
            context = RequestContext(request)
            return render_to_response('registration/registration_complete.html',{'login_form':login_form},
                                  context_instance=context)

    else:
        login_form = UserAuthenticationForm()
        context = RequestContext(request)
        
        return render_to_response('registration/registration_complete.html',{'form':login_form},
                                  context_instance=context) 
    

def activate_complete(request,  *args, **kwargs):
    if request.method == 'POST':
        login_form = UserAuthenticationForm(request, data = request.POST)
    
        if login_form.is_valid():
            auth_email = login_form.cleaned_data['username']
            user = User().objects.get(email = auth_email)
            return auth_login(request, *args, **kwargs)
        else:
			#login_form = UserAuthenticationForm()
			context = RequestContext(request)
			
			return render_to_response('registration/activation_complete.html',{'login_form':login_form},
							          context_instance=context) 			
    else:
        login_form = UserAuthenticationForm()
        context = RequestContext(request)
        
        return render_to_response('registration/activation_complete.html',{'form':login_form},
                                  context_instance=context) 

def activate(request, activation_key, template_name='registration/activate.html',
             success_url=None, extra_context=None, **kwargs):
    """
    Activate a user's account.
    """    

    activated = User().objects.activate_user(activation_key)
    if activated:
        signals.user_activated.send(sender=User(),
                                    user=activated,
                                    request=request)

        if settings.AUTHENTICATE_WHEN_ACTIVATE:
            activated.backend = 'django.contrib.auth.backends.ModelBackend'
            do_login(request, activated)

        if success_url is None:
            return redirect('registration_activation_complete', **kwargs)
        else:
            return redirect(success_url)

    if extra_context is None:
        extra_context = {}
    context = RequestContext(request)
    for key, value in extra_context.items():
        context[key] = callable(value) and value() or value

    return render_to_response(template_name,
                              kwargs,
                              context_instance=context)


class AdvertiserActivateView(LoginRequiredMixin, SuperuserRequiredMixin, UpdateView):
    """
    Class that handles to activate the advertiser Flag for the user.
    """
    
    model = User()
    template_name = "registration/advertiser_activation.html"
    context_object_name = 'user_obj'
    
    def get(self, request, *args, **kwargs):
        self.request = request
        return super(AdvertiserActivateView, self).get(request, *args, **kwargs)
        
    def get_object(self, queryset=None):
        user = super(AdvertiserActivateView, self).get_object()
        
        if not user.is_advertiser:
            
            # Flag to itendify the already activated account
            self.is_activated = False
            
        
            admin_email = get_option('admin_email', 'admin_email')
            # Send Authendication mail for the advertiser.
            to_email = admin_email
            subject = "Authendication Mail for the Advertiser!"
            
            if Site._meta.installed:
                site = Site.objects.get_current()
            else:
                site = RequestSite(self.request)
                        
            data = {
                'user_obj' : user,
                'site' : site
            }
            body_msg = render_to_string("registration/advertiser_activation_complete.html",data )
            
            if '@' in user.email:
                user_email = user.email
            else:
                user_email = user.username
                
            msg = EmailMessage(subject, body_msg, to_email, [user_email], headers = {'Reply-To': to_email})
            msg.content_subtype = "html"  
            msg.send(fail_silently=False)
        else:
            self.is_activated = True
            
        return user

    def get_context_data(self, **kwargs):
        context = super(AdvertiserActivateView,
                        self
                       ).get_context_data(**kwargs)
        
        context['is_activated'] = self.is_activated
        return context


