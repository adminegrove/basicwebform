from django import forms
from django.contrib.auth.forms import ReadOnlyPasswordHashField
from django.core.mail import send_mail, EmailMessage 
from django.shortcuts import render

from django.conf import settings
from django.contrib.auth.forms import AuthenticationForm
from django.utils.translation import ugettext_lazy as _

from django.contrib.auth import get_user_model
User = get_user_model


CHOICES = (
     ('individual', ' Individual User',), 
     ('corporate', 'Corporate User',)
 )

class UserCreationForm(forms.ModelForm):
    """
    A form that creates a user, with no privileges, from the given username and
    password.
    """

    error_messages = {
        'password_mismatch': _("The two password fields didn't match."),
    }

    email = forms.EmailField(label='Email Address', max_length=250)
    first_name = forms.CharField(label='First Name', required=True, max_length=20)
    last_name = forms.CharField(label='Last Name', required=True, max_length=12)
    password1 = forms.CharField(label='Password', min_length=6, widget=forms.PasswordInput)
    password2 = forms.CharField(label='Password confirmation', min_length=6, widget=forms.PasswordInput)
    user_type = forms.ChoiceField(choices=CHOICES)

    class Meta:
            model = User()    
            exclude = ('username', 'password', 'date_joined', 'last_login')

    def __init__(self, *args, **kwargs):
        super(UserCreationForm, self).__init__(*args, **kwargs)
        self.fields['user_type'].required = True 
        self.fields['user_type'].empty_label = "Select Job Role"
        
    def clean_password2(self):
        # Check that the two password entries match
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError(
                self.error_messages['password_mismatch'])
        return password2
            
    def save(self, commit=True):
        user = super(UserCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])

        if commit:
            user.save()
 
        return user


class UserChangeForm(forms.ModelForm):
    """A form for updateing users. Includes all the fields on
    the user, but replaces the password field with admin's
    pasword hash display field.
    """
    password = ReadOnlyPasswordHashField(label=_("Password"),
        help_text=_("Raw passwords are not stored, so there is no way to see "
                    "this user's password, but you can change the password "
                    "using <a href=\"password/\">this form</a>."))

    class Meta:
        model = User()

    def clean_password(self):
        # Regardless of what the user provides, return the initial value.
        # This is done here, rather than on the field, because the
        # field does not have access to the initial value
        return self.initial["password"]        


class UserAuthenticationForm(AuthenticationForm):
    """
    Base class for authenticating users. Extend this to get a form that accepts
    username/password logins.
    """
    
    error_messages = {
        'invalid_login': _("Please Enter the Valid Login Email Address and Password."),
        'no_cookies': _("Your Web browser doesn't appear to have cookies "
                        "enabled. Cookies are required for logging in."),
        'inactive': _("This account is inactive."),
    }
    
    remember_me = forms.BooleanField (
                                        label = _( 'Remember Me' ),
                                        initial = False,
                                        required = False,   
                                    )
    
    
class AdminUserCreationForm(forms.ModelForm):
    """
    A form that creates a user, with no privileges, from the given username and
    password.
    """

    error_messages = {
        'password_mismatch': _("The two password fields didn't match."),
    }

    email = forms.CharField(label='Email Address', max_length=250)
    password1 = forms.CharField(label='Password', min_length=6,  widget=forms.PasswordInput)
    password2 = forms.CharField(label='Password confirmation', min_length=6, widget=forms.PasswordInput)
    user_type = forms.ChoiceField(choices=CHOICES)
    
    class Meta:
        model = User()
        fields = ('user_type', )
        
    def __init__(self, *args, **kwargs):
        super(UserCreationForm, self).__init__(*args, **kwargs)
        self.fields['user_type'].required = True 
        self.fields['user_type'].empty_label = "Select Job Role"    
        
    def clean_password2(self):
        # Check that the two password entries match
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError(
                self.error_messages['password_mismatch'])
        return password2

    def save(self, commit=True):
        user = super(AdminUserCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user
    
     
class UserRegisterForm(UserCreationForm):

    class Meta:
        model = User()
        fields = ('first_name', 'last_name', 'email', 'username', 'password1', 'password2')
        
        