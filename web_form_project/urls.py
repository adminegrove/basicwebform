from django.conf.urls import patterns, include, url

from django.contrib import admin
from django.contrib.auth.views import login
from django.core.urlresolvers import reverse
admin.autodiscover()

from django.conf.urls.static import static
from django.conf import settings

# custom view
from web_form_app.views import home
from web_form_app.views import webform, device_list, txt_download,resend_activation

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'web_form_project.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^accounts/', include('registration.urls')),
    url(r'^$',home, name="index"),
    url(r'^webform/',webform, name="webform"),
    url(r'^device/list/',device_list, name="devices_list"),
    url(r'^resend/activation/mail/',resend_activation, name="resend_activation"),
    url(r'^download/(?P<id>[0-9]+)/',txt_download, name="download"),
    #url(r'', include('django.contrib.auth.urls')),
)

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
