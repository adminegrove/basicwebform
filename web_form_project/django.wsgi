import os
import sys

sys.path.insert(0, '/var/www/sites/Basic_web_form/web_form_project')
sys.path.insert(1, '/var/www/sites/Basic_web_form/web_form_project/web_form_project')
sys.path.insert(2, '/var/www/sites/Basic_web_form/venv-webform/lib/python2.7/site-packages')

os.environ['DJANGO_SETTINGS_MODULE'] = 'settings'

import django.core.handlers.wsgi
application = django.core.handlers.wsgi.WSGIHandler()

