from django import forms

from web_form_app.models import WebForm, ApplyACL, ACTION_CHIOCES
from django.contrib.auth import get_user_model


User = get_user_model


ACCESS_CHOICES = (
  ("telnet", "Telnet"),
  ("ssh", "ssh"),
  ("web", "Web"),
  
)


class webformFrom(forms.ModelForm):
    class Meta:
        model = WebForm
        exclude = ('user', 'datetime', 'slug')
 
    
    
    #vendor = forms.ChoiceField()
    #device_type = forms.ChoiceField()
    #device_name = forms.CharField(max_length=100)
    #password = forms.ChoiceField(required=False)
    #enable_encryption = forms.ChoiceField(required=False)
    #access_options = forms.ChoiceField(choices=ACCESS_CHOICES,required=False)


class VLANForm(forms.Form) :
    vlan_id= forms.CharField(max_length=4, required=False,widget = forms.TextInput(attrs={'class': 'layer2_vlan_id'}))
    vlan_name = forms.CharField(max_length=100, required=False,widget = forms.TextInput(attrs={'class': 'layer2_vlan_name'}))
    

class ApplyACLForm(forms.ModelForm):
    class Meta:
        model = ApplyACL
        widgets = {
                'source_ip' : forms.TextInput(attrs={'placeholder': '10.1.1.1/24'}),
                'source_port' : forms.TextInput(attrs={'placeholder': 'TCP/ANY'}),
                'destination_ip' : forms.TextInput(attrs={'placeholder': '10.1.1.1/24'}),
                'destination_port'  : forms.TextInput(attrs={'placeholder': 'TCP/ANY'}),
                'source_id' : forms.TextInput(attrs={'placeholder': '10'}),
                'apply_to'  : forms.TextInput(attrs={'placeholder': 'Gig0/1:IN'})}
        
    def __init__(self,*args, **kwargs):
        super(ApplyACLForm, self).__init__(*args, **kwargs)
        self.fields['action'].initial = ACTION_CHIOCES[0][0]
    
    
class ResendForm(forms.Form):
    resend_email_id = forms.EmailField()
    
    def clean_resend_email_id(self):
        try:
            user = User().objects.get(email__iexact=self.cleaned_data['resend_email_id'])
            return self.cleaned_data['resend_email_id']
        except :
            raise forms.ValidationError((u'Email id is not associated with any user. Kindly check with different mail id.'))
    
    
