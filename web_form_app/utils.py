#import yaml
from django.conf import settings


def create_yaml(request, web_form):
    """
    Write the Device Details in YAML File.
    """
    with open(settings.YAML_DIR + '/webform_{}_{}.yaml'.format(request.user, web_form.id),'w') as yaml_file:
        for vlan in web_form.vlan_name_id.all():
            yaml_dump_obj = {vlan.vlan_id:{
                'vlan_id': vlan.vlan_id,
                'vlan_name': vlan.vlan_name,
                'device_name': web_form.device_name,
                'password': web_form.password,
                'access_option':web_form.access_options}
            }
            wt_obj = yaml.dump(yaml_dump_obj)
            yaml_file.write(wt_obj)
            
    return yaml_file


def create_txt(request, web_form):
    """
    Write the Device Details in YAML File.
    """
    with open(settings.TXT_DIR + '/webform_{}_{}.txt'.format(request.user, web_form.id),'w') as text_file:
        for vlan in web_form.vlan_name_id.all():
            text_dump_obj = {vlan.vlan_id:{
                'vlan_id': vlan.vlan_id,
                'vlan_name': vlan.vlan_name,
                'device_name': web_form.device_name,
                'password': web_form.password,
                'access_option':web_form.access_options}
            }
            text_file.write(str(text_dump_obj))
            
    return text_file
