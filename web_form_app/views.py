from datetime import datetime, date

from django.shortcuts import render,render_to_response
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.core.context_processors import csrf
from django.conf import settings
from django.template.loader import render_to_string
from django.forms.formsets import formset_factory
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.contrib.sites.models import RequestSite, Site

from registration import signals
from registration.forms import UserRegisterForm
from forms import webformFrom, VLANForm, ResendForm, ApplyACLForm
from models import WebForm, Vlan, ApplyACL
from utils import create_yaml, create_txt

from django.contrib.auth import get_user_model
User = get_user_model

# Create your views here.

def home(request):
	return render (request,'index.html',{})


@login_required
def webform(request):
	confg_form = webformFrom()
	vlan_formset = formset_factory(VLANForm)
	apply_acl_formset = formset_factory(ApplyACLForm)

	if request.method == 'POST':
		formset = vlan_formset(request.POST)
		apply_acl_forms = apply_acl_formset(request.POST)
		vlan_id_list = []
		vlan_name_list = []

		# Get the Details of  WebForm
		vender = request.POST.get('vendor')
		device_type = request.POST.get('device_type')
		device_name = request.POST.get('device_name')

		if device_name.strip() == "":
			vlan_formset = formset_factory(VLANForm)
			message = "This field is required"			
			return render(request,'web_form.html',{'formset': vlan_formset(),
			                                       'apply_acl_formset': apply_acl_formset(),
			                                       'vender_form':confg_form,
			                                       'message': message
			                                       }
			              )			

		pwd = request.POST.get('password')
		access_option = request.POST.get('access_options')
		enable_encryption = request.POST.get('enable_encryption')
		layer2_qos = request.POST.get('layer2_qos')
		layer2_qos_types = request.POST.get('layer2_qos_types')
		layer3_qos = request.POST.get('layer3_qos')
		layer3_qos_types = request.POST.get('layer3_qos_types')
		layer2_qos_types_text = request.POST.get('qos_custom1_text')
		layer3_qos_types_text = request.POST.get('qos_custom2_text')
		mamt_vlan_id = request.POST.get('mamt_vlan_id')
		ip_address = request.POST.getlist('ip_address')
		mgmt_ip_addr_type = request.POST.get('mgmt_ip_addr_type')
		ifdsc_text = request.POST.getlist('ifdsc_text')
		ip_addr = request.POST.getlist('ip_addr')
		ifdsc_ph = request.POST.getlist('ifdsc_ph')
		phy_ip_add = request.POST.getlist('phy_ip_add')
		phy_ifdsc_text = request.POST.getlist('phy_ifdsc_text')
		created_datetime = datetime.today().now()
		# Save the row in Web Form Table
		web_form = WebForm.objects.create(user = request.user,
		                                  vender = vender,
		                                  device_type = device_type,
		                                  datetime = created_datetime,
		                                  device_name = device_name,
		                                  password = pwd,
		                                  access_options = access_option,
		                                  enable_encription = enable_encryption
		                                  )

		web_form.mgmt_ip_addr_type = mgmt_ip_addr_type
		web_form.mgmt_ip_addr = ip_address
		web_form.mgmt_vlan_id = mamt_vlan_id

		web_form.layer2_qos = layer2_qos
		web_form.layer2_qos_types = layer2_qos_types
		web_form.layer3_qos = mgmt_ip_addr_type
		web_form.layer3_qos_types = layer3_qos_types
		if layer2_qos_types == "No":
			web_form.layer2_qos_types_text = layer2_qos_types_text
		if layer3_qos_types == "No":
			web_form.layer3_qos_types_text = layer3_qos_types_text		
		web_form.save()
		# Getting apply_acl form data
		for i in range(0,100):
			source_ip = request.POST.get('form-'+str(i)+'-source_ip',False)
			source_port = request.POST.get('form-'+str(i)+'-source_port',False)
			destination_ip = request.POST.get('form-'+str(i)+'-destination_ip',False)
			destination_port = request.POST.get('form-'+str(i)+'-destination_port',False)
			source_id = request.POST.get('form-'+str(i)+'-source_id',False)
			action = request.POST.get('form-'+str(i)+'-action',False)
			apply_to = request.POST.get('form-'+str(i)+'-apply_to',False)
			vlan_id = request.POST.get('form-'+str(i)+'-vlan_id',False)
			vlan_name = request.POST.get('form-'+str(i)+'-vlan_name',False)
			ip_addr = request.POST.get('form-'+str(i)+'-ip_addr',False)
			ip_dsc = request.POST.get('form-'+str(i)+'-ip_dsc',False)
			if source_ip or source_port or destination_ip or destination_port or source_id or apply_to:
				apply_acl = ApplyACL.objects.create(source_ip =source_ip,
					                                                    source_port = source_port,
					                                                    destination_ip = destination_ip,
					                                                    destination_port = destination_port,
					                                                    source_id = source_id,
					                                                    action =action,
					                                                    apply_to = apply_to)
				web_form.apply_acl.add(apply_acl)
				web_form.save()
			if vlan_id or vlan_name or ip_addr or ip_dsc :
				vlan = Vlan.objects.create(vlan_id = vlan_id,
								                       vlan_name = vlan_name,
								                       ip_addr = ip_addr,
								                       ip_dsc = ip_dsc)
				web_form.vlan_name_id.add(vlan)
				web_form.save()
		return HttpResponseRedirect('/device/list/')
	else:	
		dump_row = WebForm()
		vlan_formset = formset_factory(VLANForm)

	return render(request,'web_form.html',{'formset': vlan_formset(),
	                                       'vender_form':confg_form,
	                                       'apply_acl_formset': apply_acl_formset()
	                                       }
	              )


@login_required
def device_list(request):
	# Get the list of Devices and User
	web_forms= WebForm.objects.filter(user = request.user)
	return render(request,'device_list.html',{'web_forms': web_forms})


@login_required
def txt_download(request, **kwargs):
	web_form_id  = kwargs['id']
	web_form = WebForm.objects.get(id = web_form_id)

	#file_name = settings.TXT_DIR + '/{}_{}.txt'.format(web_form.slug, date.today())
	file_name = '{}_{}.txt'.format(web_form.slug, date.today())
	rendered = render_to_string('download.txt', {'web_form': web_form})

	response = HttpResponse(rendered, content_type='text/txt')
	response['Content-Disposition'] = 'attachment; filename={}'.format(file_name)
	return response


def resend_activation(request):
	if request.method == 'POST':
		form = ResendForm(request.POST)
		if form.is_valid():
			email = request.POST['resend_email_id']
			new_user = User().objects.get(email= email )
			if Site._meta.installed:
				site = Site.objects.get_current()
			else:
				site = RequestSite(request)		

			User().objects.send_activation_email(new_user, site )
			#return HttpResponseRedirect('/accounts/register/complete/')
			return HttpResponseRedirect(reverse('registration_complete'))
		else:
			return render(request,'registration/resend_email.html',{'resend_form': form})		


	else:
		resend_form = ResendForm()
		return render(request,'registration/resend_email.html',{'resend_form': resend_form})
