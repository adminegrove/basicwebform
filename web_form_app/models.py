from django.db import models
from django.contrib.auth import get_user_model

from django_extensions.db.fields import AutoSlugField

User = get_user_model()


ACCESS_CHIOCES =  (
    ('telnet', 'Telnet'),
    ('ssh', 'SSH' ),
    ('web', 'Web')
)

ACTION_CHIOCES =  (
    ('permit', 'Permit'),
    ('deny', 'Deny' ))


class Vlan(models.Model):
    vlan_id = models.CharField(max_length = 100,null = True, blank = True)
    vlan_name = models.CharField(max_length = 100,null = True, blank = True)
    ip_addr = models.CharField(max_length = 100,null = True, blank = True)
    ip_dsc = models.CharField(max_length = 100,null = True, blank = True)
    
    def __unicode__(self):
        return "%s - %s" % (self.vlan_id, self.vlan_name)


class ApplyACL(models.Model):
    source_ip = models.CharField("Source IP/mask", max_length = 30)
    source_port = models.CharField("Source Proto/Port", max_length = 30)
    destination_ip = models.CharField("Destination  IP/mask", max_length = 30)
    destination_port = models.CharField("Destination Proto/Port", max_length = 30)
    source_id = models.CharField("Source ID", max_length = 10,null = True, blank = True)
    action = models.CharField("Action",  choices=ACTION_CHIOCES, max_length = 100)
    apply_to = models.CharField("Apply To", max_length = 100,null = True, blank = True)
    
    def __unicode__(self):
        return "%s - %s" % (self.source_ip, self.destination_ip)

                           
class WebForm(models.Model):
    user = models.ForeignKey(User)
    vender = models.CharField(max_length = 100)
    device_type = models.CharField(max_length = 100)
    datetime  = models.DateTimeField(auto_now=True)
    device_name = models.CharField(max_length = 100)
    slug = AutoSlugField("Device Name Slug",
                            max_length=100,
                            editable=True,
                            populate_from='device_name',
                            unique=True,
                            separator="-",
                            blank=False
                        )
    password = models.CharField(max_length = 50,null = True, blank = True)
    enable_encription = models.CharField(max_length = 50,null = True, blank = True)
    access_options = models.CharField(max_length=50, choices=ACCESS_CHIOCES,null = True, blank = True)
    vlan_name_id = models.ManyToManyField(Vlan,null = True, blank = True)
    
    confi_layer = models.CharField(max_length=50, null = True, blank = True)
    confi_desc = models.CharField(max_length=50, null = True, blank = True)
    
    layer2_qos = models.NullBooleanField()
    layer2_qos_types = models.NullBooleanField()
    layer2_qos_types_text = models.TextField(null = True, blank = True)
    
    layer3_qos = models.NullBooleanField()
    layer3_qos_types = models.NullBooleanField()
    layer3_qos_types_text = models.TextField(null = True, blank = True)
    
    mgmt_ip_addr_type = models.CharField(max_length = 100, null = True, blank = True)
    mgmt_ip_addr = models.CharField(max_length = 100, null = True, blank = True)
    mgmt_vlan_id = models.CharField(max_length = 100, null = True, blank = True)
    apply_acl = models.ManyToManyField(ApplyACL,null = True, blank = True)
    
    def __unicode__(self):
        return "%s - %s" % (self.user, self.device_name)

